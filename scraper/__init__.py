"""
SCK watersports website scraper
"""
__author__ = 'Peter Bittner'
__email__ = 'peter@painless.software'
__url__ = 'https://gitlab.com/sckreuzlingen/scrape-data'
__version__ = '1.0.0'
__license__ = 'GPLv3'
