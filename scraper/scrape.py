"""
The actual scraping of websites.
"""
from datetime import datetime
import sys

from bs4 import BeautifulSoup
import requests


def waterpolo_matches(league='166-2018-2019-national-league-a',
                      team='11-sc-kreuzlingen'):
    """
    Extract season league matches from Swiss Waterpolo team page
    """
    base = 'https://results.swisswaterpolo.com'
    url = f"{base}/men/calendar-layout/team/{league}/{team}"
    object_csspath = '#joomsport-container .jsMatchDivMain .jstable-row'

    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    rows = soup.select(object_csspath)

    for row in rows:
        try:
            date_time = row.select('.jsMatchDivTime div')[0].text.strip()
            home = row.select('.jsMatchDivHome div')[0].text.strip()
            guest = row.select('.jsMatchDivAway div')[0].text.strip()
            score = row.select('.jsMatchDivScore div')[0].text
            venue = row.select('.jsMatchDivVenue a')[0].text.strip()
        except IndexError:
            print(f"Ignoring {row}", file=sys.stderr)
        else:
            try:
                dt = datetime.strptime(date_time, '%d.%m.%Y %H:%M')
            except ValueError:
                print(f"Can't parse DateTime {date_time}", file=sys.stderr)
                weekday = '??'
                date = date_time if date_time.strip() else '--.--.----'
                time = date_time if date_time.strip() else '--:--'
            else:
                day_name = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So']
                weekday = day_name[dt.weekday()]
                date = dt.date().strftime('%d.%m.%Y')
                time = dt.time().strftime('%H:%M')

            line = {
                'date': date,
                'day': weekday,
                'time': time,
                'home': home,
                'guest': guest,
                'score': score.replace(
                    ' - ', '\t:\t').replace(
                    ' v ', '-\t:\t-'),
                'venue': venue,
            }
            print('\t'.join(line.values()))
