"""
CLI implementation for SCK scraper.
"""
import click

from requests.exceptions import RequestException

from . import scrape


@click.version_option()
@click.command()
def scraper():
    """A website scraper"""
    scrape.waterpolo_matches()


def main():
    """Main entry point for the CLI"""
    try:
        scraper()
    except RequestException as req:
        raise SystemExit(req)
    except Exception as other:
        raise SystemExit(other)


if __name__ == '__main__':
    main()
