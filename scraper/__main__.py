"""
Entry point for when run as a module: python3 -m scrape
"""
from .cli import main

if __name__ == '__main__':
    main()
