Scrape data from watersports websites
=====================================

- https://realpython.com/python-web-scraping-practical-introduction/

Usage
-----

Install and run it:

.. code-block::

    $ python3 setup.py install
    $ sck-scraper

Or run it as a module:

.. code-block::

    $ python3 -m scraper > timetable.csv

or (silence parsing issues):

.. code-block::

    $ python3 -m scraper 2> /dev/null

Testing
-------

.. code-block::

    $ tox
