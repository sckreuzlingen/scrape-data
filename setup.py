#!/usr/bin/env python3
"""
Packaging setup for SCK scraper.
See: https://click.palletsprojects.com/en/7.x/setuptools/
"""
from os.path import abspath, dirname, join
from setuptools import find_packages, setup

import scraper as package


def read_file(filename):
    """Get the contents of a file"""
    with open(join(abspath(dirname(__file__)), filename)) as file:
        return file.read()


setup(
    name='sck-scraper',
    version=package.__version__,
    license=package.__license__,
    author=package.__author__,
    author_email=package.__email__,
    description=package.__doc__.strip(),
    long_description=read_file('README.rst'),
    long_description_content_type='text/x-rst',
    url=package.__url__,
    packages=find_packages(exclude=['test*']),
    include_package_data=True,
    keywords=['cli', 'scrape'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    python_requires='>=3.6',
    install_requires=read_file('requirements.in'),
    entry_points={
        'console_scripts': [
            'sck-scraper = scraper.cli:main',
        ],
    },
)
