"""
Tests for the command line interface (CLI)
"""
import os
import pytest

from click.testing import CliRunner
from requests.exceptions import RequestException
from unittest.mock import patch

import scraper.cli


def launch_cli(*args):
    """
    Helper for testing the click CLI.
    See https://click.palletsprojects.com/en/7.x/testing/
    """
    runner = CliRunner()
    result = runner.invoke(scraper.cli.scraper, args)
    return result


def test_entrypoint():
    """
    Is entrypoint script installed? (setup.py)
    """
    exit_status = os.system('sck-scraper --version')
    assert exit_status == 0


@patch('scraper.cli.scrape', side_effect=RequestException)
def test_handle_requests_errors(mock_scraper):
    """
    Are exceptions from requests handled correctly? (catch + print + exit)
    """
    with pytest.raises(SystemExit):
        scraper.cli.main()


@patch('scraper.cli.scrape', side_effect=Exception)
def test_handle_other_errors(mock_scraper):
    """
    Are any other exceptions handled correctly? (catch + print + exit)
    """
    with pytest.raises(SystemExit):
        scraper.cli.main()
